/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {TypeChatServer} from "../core/TypeChatServer";
import {BaseRoomManager} from "./base/BaseRoomManager";

export class RoomManager extends BaseRoomManager{

    constructor(public server:TypeChatServer){
        super(server);
    }

}