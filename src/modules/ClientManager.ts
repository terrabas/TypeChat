/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {BaseClientManager} from "./base/BaseClientManager";
import {TypeChatServer} from "../core/TypeChatServer";
import {Client} from "./Client";

export class ClientManager extends BaseClientManager{

    constructor(public server:TypeChatServer){
        super(server);
    }

    createNewClient(clientSocket:any, server:TypeChatServer){
        return new Client(clientSocket, server);
    }
}