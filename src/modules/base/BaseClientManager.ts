/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {ConsoleLog} from "../../helpers/ConsoleLog";
import * as _ from 'underscore';
import {User} from "../../models/base/User";
import {ArrayHelper} from "../../helpers/ArrayHelper";
import {TypeChatServer} from "../../core/TypeChatServer";
import {BaseClientModule} from "../../core/modules/BaseClientModule";
import {ModuleType} from "../../core/modules/BaseModuleInterface";
import {BaseClient} from "./BaseClient";
import {BaseMessage} from "../../models/base/BaseMessage";

export class BaseClientManager implements BaseClientModule {
    static moduleType: ModuleType = ModuleType.Client;

    Logger: ConsoleLog;
    clients: BaseClient[] = [];

    constructor(public server: TypeChatServer) {
        this.Logger = new ConsoleLog("ClientManager");
        this.Logger.debug("Module init!");
    }

    register(client: BaseClient) {
        if (this.isRegistered(client)) {
            this.Logger.warning("Client already registered! Skipping...");
            return false;
        }
        this.Logger.info("Client " + client.clientSocket.id + " added to list of active clients");
        this.clients.push(client);
        return true;
    }

    isRegistered(client: BaseClient) {
        // Check if client with this socket is already in the client list.
        return _.findWhere(this.clients, {clientSocket: client.clientSocket});
    }

    unregister(client: BaseClient) {
        if (ArrayHelper.remove(this.clients, client))
            this.Logger.debug("Client removed.");
        else
            this.Logger.warning("Client to be removed could not be found!");
    }

    getUserList() {
        let users: User[] = [];
        for (let client of this.clients) {
            // Skip users already in list.
            if(ArrayHelper.isEmpty(_.where(users,{id:client.user.id})))
                users.push(client.user);
        }
        return users;
    }

    userConnected(userID: number): boolean {
        return !ArrayHelper.isEmpty(_.where(this.getUserList(), {id: userID}));

    }

    getUser(userID: number): User {
        for (let user of this.getUserList()){
            if(user.id == userID)
                return user;
        }
        return null;
    }

    getClients(userID: number):BaseClient[]{
        let clients: BaseClient[] = [];
        for(let client of this.clients){
            if(client.user.id == userID)
                clients.push(client);
        }
        return clients;
    }

    broadcastToClients(clients: BaseClient[], message:BaseMessage){
        for (let client of clients){
            client.sendMessage(message);
        }
    }

    broadcastUserUpdate(user: User) {
        this.server.broadcast('user update', user);
    }

    createNewClient(clientSocket: any, server: TypeChatServer) {
        return new BaseClient(clientSocket, server);
    }

    lastClientofUserInRoom(client:BaseClient, roomID:number):boolean{
        let clients = [];
        for(let _client of this.server.moduleManager.roomModule.getRoomClients(roomID)){
            if(client.user.id == _client.user.id)
                clients.push(_client);
        }

        return clients.length == 1 && clients[0] == client;
    }
}