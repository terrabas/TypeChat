/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {TypeChatServer} from "../../core/TypeChatServer";
import {ConsoleLog} from "../../helpers/ConsoleLog";
import {User} from "../../models/base/User";
import {Room} from "../../models/base/Room";
import {ModuleType} from "../../core/modules/BaseModuleInterface";
import {BaseMessage} from "../../models/base/BaseMessage";
import {Config} from "../../core/Config";
import {ServerError} from "../../models/base/ServerError";
import {MessageHandler} from "../../core/MessageHandler";
import {MessageAck} from "../../models/base/MessageAck";
import {ArrayHelper} from "../../helpers/ArrayHelper";
import * as _ from 'underscore';

export class BaseClient {
    static moduleType: ModuleType = ModuleType.None;

    Logger: ConsoleLog;
    user: User;
    rooms: Room[] = [];

    isAuthing: boolean;

    constructor(public clientSocket: any, public server: TypeChatServer) {
        this.Logger = new ConsoleLog("CLIENT | " + clientSocket.id);
        if (server.moduleManager.clientModule.isRegistered(this))
            this.Logger.warning("Client has already been registered! This could lead to unexpected behavior!");
        this.Logger.info("Sending server info...");
        this.send("info", {
            version: Config.version,
            build: Config.build,
            protocolVersion: Config.protocolVersion,
            requiresAuth: true
        });
        this.Logger.info("Handling new client, now awaiting auth...");
        this.bind('auth', this.onAuth.bind(this));
    }

    bind(keyword: string, fnc: any) {
        this.clientSocket.on(keyword, fnc.bind(this));
    }

    send(event: string, data?: any) {
        if (data)
            this.clientSocket.emit(event, data);
        else
            this.clientSocket.emit(event);
    }

    onAuth(data: any) {
        try {
            this.Logger.info("Authenticating...");
            if (this.isAuthing) {
                this.send('auth fail', "Last auth request still pending");
                this.Logger.info("Dropped auth request: Last request still pending");
                return;
            }
            this.isAuthing = true;
            this.send('auth wait', "Authenticating...");

            // Shoot after given delay
            setTimeout(() => {
                this.server.moduleManager.authModule.authenticate(data, (userData) => {
                    this.Logger.info("Auth success!");
                    this.isAuthing = false;
                    this.server.releaseAuthSlot();
                    this.user = new User(userData);
                    this.send('auth ok', this.user);
                    this.send('setup start');
                    this.Logger.moduleName = "CLIENT | " + this.clientSocket.id + " | " + this.user.username;
                    this.Logger.info("Client now identifies as user " + this.user.username + " with ID #" + this.user.id);
                    this.server.moduleManager.clientModule.register(this);
                    this.onConnected();
                }, (reason) => {
                    this.Logger.warning("Auth failed: " + reason);
                    this.send('auth fail', reason);
                })
            }, this.server.requestAuthSlot()*1000);
        } catch (e) {
            if (this.isAuthing)
                this.server.releaseAuthSlot();
            this.Logger.panic("Authentication panic! Something happened while trying to authenticate!");
            this.Logger.error(e.stack);
        }
    }

    // Called after client has been successfully authenticated and added to the server client list
    onConnected() {
        this.bind('disconnect', this.onDisconnected);
        this.clientSetup();
        this.bind('message send', this.onMessage);
    }

    // Called when the client disconnects
    onDisconnected() {
        this.Logger.info("Client disconnected. Cleaning up...");
        this.server.moduleManager.clientModule.unregister(this);
    }

    onMessage(message: BaseMessage, ack: any) {
        this.Logger.debug("Message received: " + JSON.stringify(message));
        MessageHandler.handleMessage(message, this, ack);
    }

    sendError(error: ServerError) {
        this.send("server error", error);
        this.Logger.warning(error.message + ": " + error.description);
    }

    sendMessage(message: BaseMessage) {
        this.Logger.debug("Sending message to client.");
        this.send("message", message);
    }

    sendMsgAck(originalMessage: BaseMessage, serverMessage: BaseMessage, ack_socket: any) {
        let ack = new MessageAck({
            originalID: originalMessage.id,
            newID: serverMessage.id,
            timestamp: serverMessage.timestamp,
        });

        if (ack_socket == null) {
            this.sendError(new ServerError("msg_no_id_ack_failed", "Message successfully transmitted but could not be acknowledged for sender: Ack_socket missing!"));
            return;
        }

        ack_socket(ack);

        this.Logger.debug("Message id " + ack.newID + " acknowledged for " + this.user.username);
    }

    // Called on successful connection and auth. Setting up the client with details about rooms it's in
    clientSetup() {
        this.joinDefaultRooms();
        this.joinUserRooms();
        this.send('setup complete');
    }

    joinDefaultRooms() {
        this.Logger.info("Joining default rooms...");
        for (let room of this.server.moduleManager.roomModule['getDefaultRooms']()) {
            this.joinRoom(room);
        }
        this.Logger.info("Done joining default rooms");
    }

    joinUserRooms() {
        this.Logger.info("Joining user rooms...");
        for (let room of this.server.moduleManager.roomModule['getUserRooms']()) {
            this.joinRoom(room);
        }
        this.Logger.info("Done joining user rooms...")
    }

    joinRoom(room: Room) {
        this.Logger.info("Joining room " + room.name);
        this.rooms.push(room);
        this.server.moduleManager.roomModule.addClientToRoom(this, room);
        this.send('room add', room);
        this.Logger.info("Sending welcome message...");
        this.send('message', BaseMessage.anonymousRoomMessage(room, room.description));
        // Room joined message
    }

    leaveRoom(room: Room) {
        this.Logger.info("Leaving room "+ room.name);
        ArrayHelper.remove(this.rooms, room);
        this.send('room remove', room.id);
        if(this.server.moduleManager.clientModule.lastClientofUserInRoom(this, room.id)){
            room.removeUser(this.user.id);
            this.Logger.debug("Last client of user disconnected! User removed from room list.");
        }
    }

    hasJoinedRoom(roomID){
        return !ArrayHelper.isEmpty(_.where(this.rooms, {id: roomID}));
    }
}