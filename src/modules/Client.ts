/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {TypeChatServer} from "../core/TypeChatServer";
import {BaseClient} from "./base/BaseClient";

export class Client extends BaseClient{

    constructor(clientSocket:any, server:TypeChatServer){
      super(clientSocket,server)
    }
}