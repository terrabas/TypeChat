/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

/**
 *                        TypeChat
 *                      © 2018 Terra
 *
 */
import {ConsoleLog} from "./helpers/ConsoleLog";
import {TypeChatServer} from "./core/TypeChatServer";
import * as jsonminify from "jsonminify";
import * as fs from "fs";
import * as path from "path";


let Logger = new ConsoleLog("TypeChat");
let cmd = undefined;
try {
    cmd = process.argv[2];
} catch (e) {
    // ignore commands
}




Logger.info("-- TypeChat NodeJS Engine --");
let Typechatserver;
try {
    var config;
    global['appRoot'] = path.resolve(__dirname);

    try { config = fs.readFileSync(__dirname+'/config/config.json', 'utf8').toString()} catch (e) {
        throw new Error("Config file read error: "+e);
    }
    if(cmd != undefined){
        switch (cmd) {
            case "-t":
                Logger.info("Testmode!");
                Logger.info("T E S T I N G   I N I T I A L I Z A T I O N");
                Typechatserver = new TypeChatServer(JSON.parse(jsonminify(config)));
                Typechatserver.init(()=>{Logger.success("Initialization completed without errors!");});
                break;
        }
    } else {
        Typechatserver = new TypeChatServer(JSON.parse(jsonminify(config)));
        let readyCallback = function () {
            try {
                Typechatserver.start();
            } catch (e) {
                Logger.panic("TypeChat start failed! See stack below for more info.");
                Logger.error(e.stack);
                process.exit(1);
            }
        };
        Typechatserver.init(readyCallback);
    }
} catch (e) {
    Logger.panic("TypeChat initialisation failed! See stack below for more info.");
    Logger.error(e.stack);
    process.exit(1);
}



