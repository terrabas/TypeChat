/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

export class MessageAck {
    originalID:string;
    newID:string;
    timestamp:number;

    constructor(data:any){
        this.originalID = data.originalID;
        this.newID = data.newID;
        this.timestamp = Date.now();
    }
}