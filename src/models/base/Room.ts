/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {BaseMessage} from "./BaseMessage";
import {User} from "./User";

export class Room {
    id: number;
    name: string;
    description: string;
    creator: number;
    default_join: boolean;
    memberRoles: string;
    users: User[] = [];
    messages: BaseMessage[] = [];

    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.description = data.description;
        this.creator = data.creator;
        this.default_join = data.default_join;
        this.memberRoles = data.memberRoles;
        this.messages = [];
    }

    removeUser(userID: number): boolean {
        // @ts-ignore
        let index = this.users.findIndex(function (o) {
            return o.id == userID;
        });
        if (index !== -1) {
            this.users.splice(index, 1);
            return true;
        }
        return false;
    }
}