/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {Room} from "./Room";
import {User} from "./User";
import {GeneralHelpers} from "../../helpers/GeneralHelpers";

export enum MessageDestinationSourceType{
    Room = "room",
    User = "user",
    Whisper = "whisper",
    System = "system"
}

interface MessageDestination {
    type:MessageDestinationSourceType;
    id:number;
}

export class BaseMessage {
    id:string;
    content:string;
    source:MessageDestination;
    destination:MessageDestination;
    timestamp:number;

    constructor(data:any){
        this.id = data.id;
        this.timestamp = data.timestamp;
        this.destination = data.destination;
        this.source = data.source;
        this.content = data.content;
    }

    storageMinify():BaseMessage{
        // @ts-ignore
        let newMsg = Object.assign({},this);
        delete newMsg.destination;
        return newMsg;
    }

    static createSystemMessage(){
        return new BaseMessage({
            id: 0,
            source: {type:MessageDestinationSourceType.System},
            content: "",
            //fromCharacter: 0,
            //timestamp: Date.now(),
            destination: {
                type: 'whisper',
                id: -1
            }
        });
    }

    static userRoomMessage(originalMessage:BaseMessage, user:User){
        return new BaseMessage({
            id: GeneralHelpers.makeID(),
            timestamp: Date.now(),
            source: {
                type:MessageDestinationSourceType.User,
                id:user.id
            },
            content: originalMessage.content,
            destination: originalMessage.destination,
        });
    }

    static userWhisperMessage(originalMessage:BaseMessage, user:User){
        return this.userRoomMessage(originalMessage, user);
    }


    static anonymousRoomMessage(room:Room, content?:any){
        let message = this.createSystemMessage();
        message.source.type = MessageDestinationSourceType.System;
        message.source.id = room.id;
        if(content)
            message.content = content;
        return message;
    }
}