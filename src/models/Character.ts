/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

export class Character {
    id:number;
    name:string;
    bio:string;
    gender:string;
    species:string;
    age:number;
    location:string;
    rating:string;
    avatar_url:string;
    user_id:number;

    constructor(data){
        this.id = data.id;
        this.bio = data.bio;
        this.gender = data.gender;
        this.species = data.species;
        this.age = data.age;
        this.location = data.location;
        this.name = data.name;
        this.rating = data.rating;
        this.avatar_url = data.avatar_url;
        this.user_id = data.user_id;
    }
}