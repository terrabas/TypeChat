/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

export class Formatter {
    static formatSeconds(seconds:number){
        function pad(s){
            return (s < 10 ? '0' : '') + s;
        }
        let hours = Math.floor(seconds / (60*60));
        let minutes = Math.floor(seconds % (60*60) / 60);
        seconds = Math.floor(seconds % 60);

        return pad(hours) + ':' + pad(minutes) + ':' + pad(seconds);
    }
}