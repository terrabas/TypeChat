/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import colors = require("colors");
import {Config} from "../core/Config";
import {Logger} from "ts-log-debug";

export class ConsoleLog {
    logger;

    constructor(public moduleName:string){
        this.logger = new Logger(moduleName);

        this.logger.appenders
            .set("file-log",{
                type: "file", maxLogSize:10, levels:["error","fatal","warn"], filename: "logfile.log",
                layout: {
                    type: "pattern",
                    pattern: "%d %p %m"
                }
            })
            .set("console-log",{
                type: "console", layout: { type: "messagePassThrough" }
            })
    }

    debug(message:string){
        if(Config.config.debug)
            this.logger.debug("["+this.moduleName+"]: "+colors.white(message));
    }

    verbose(message:string){
        if(Config.config.verbose)
            this.logger.debug("["+this.moduleName+"]: "+colors.gray(message));
    }

    info(message:string){
        this.logger.info("["+this.moduleName+"]: "+colors.cyan(message));
    }

    success(message:string){
        this.logger.info("["+this.moduleName+"]: "+colors.green(message));
    }

    startupinfo(message:string){
        console.log("["+this.moduleName+"] -> "+colors.green(message));
    }

    twotabbedlog(message:string){
        console.log("      "+colors.green(message));
    }

    warning(message:string){
        this.logger.warn(colors.yellow("["+this.moduleName+"]: "+message));
    }

    error(message:string){
        this.logger.error(colors.red("["+this.moduleName+"]: "+message));
    }

    panic(message:string){
       this.logger.fatal(colors.bgRed("PANIC ["+this.moduleName+"]: "+message));
    }

}