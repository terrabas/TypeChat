/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

export class ArrayHelper {
    static remove(array:any, key:any){
        var index = array.indexOf(key, 0);
        if (index > -1) {
            array.splice(index, 1);
            return true;
        } else {
            return false;
        }
    }

    static isEmpty(array:any){
        return !(typeof array !== 'undefined' && array.length > 0);
    }


}