/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {BaseMessage, MessageDestinationSourceType} from "../models/base/BaseMessage";
import {BaseClient} from "../modules/base/BaseClient";
import {PermissionManager} from "./PermissionManager";
import {ServerError} from "../models/base/ServerError";

export class MessageHandler {

    static handleMessage(message: BaseMessage, sourceClient: BaseClient, ack:any): void {
        let server = sourceClient.server;
        if (!PermissionManager.check.preMessageProcessing(message, sourceClient)) {
            sourceClient.sendError(new ServerError("perm_msg_drop", "Permission to send this message has been denied. Message dropped."));
            return;
        }
        if (message.destination.id == null) {
            sourceClient.sendError(new ServerError("msg_syntx_invld_dest_id", "Message syntax invalid: Destination ID missing or not valid."));
            return;
        }

        if(message.content == ""){
            sourceClient.sendError(new ServerError('msg_no_content',"Message has no content. Dropped."));
            return;
        }

        // TODO: throttling

        let NewMessage;
        switch (message.destination.type) {
            case MessageDestinationSourceType.Room:
                if (!server.moduleManager.roomModule.exists(message.destination.id)) {
                    sourceClient.sendError(new ServerError("msg_syntx_invld_dest_id", "Message syntax invalid: Destination ID missing or not invalid."));
                    return;
                }

                if(!sourceClient.hasJoinedRoom(message.destination.id)){
                    sourceClient.sendError(new ServerError("dest_room_not_member", "Client is not a member of the destination room."));
                    return;
                }

                if(!PermissionManager.check.canUserSendRoomMessage(sourceClient.user,message.destination.id)){
                    sourceClient.sendError(new ServerError("perm_room_denied", "You are not allowed to send a message to this room."));
                    return;
                }

                NewMessage = BaseMessage.userRoomMessage(message,sourceClient.user);
                server.moduleManager.roomModule.broadcastRoomMessage(NewMessage, sourceClient, true);
                break;
            case MessageDestinationSourceType.Whisper:
                if(!server.moduleManager.clientModule.userConnected(message.destination.id)){
                    sourceClient.sendError(new ServerError("msg_dest_usr_invld","Destination ID missing or incorrect. Is the user online?"));
                    return;
                }

                if(!PermissionManager.check.canUserSendWhisper(sourceClient.user, server.moduleManager.clientModule.getUser(message.destination.id))){
                    sourceClient.sendError(new ServerError("perm_whisper_denied","Permission to send a whisper to this user has been denied."));
                    return;
                }

                NewMessage = BaseMessage.userWhisperMessage(message, sourceClient.user);
                server.moduleManager.clientModule.broadcastToClients(server.moduleManager.clientModule.getClients(NewMessage.destination.id), NewMessage);
                break;
            default:
                sourceClient.sendError(new ServerError("msg_syntx_invld_dest", "Message syntax invalid: Destination type unknown!"));
                return;
        }
        if(NewMessage != null)
            sourceClient.sendMsgAck(message, NewMessage, ack);

    }


}