/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {ConsoleLog} from "../helpers/ConsoleLog";
import {ConnectionHandler} from "./ConnectionHandler";
import {Config} from "./Config";
import {ModuleManager} from "./ModuleManager";
import {PermissionManager} from "./PermissionManager";
import {BasePermissions} from "../modules/base/BasePermissions";
import {Formatter} from "../helpers/Formatter";
import {BackendCom} from "./BackendCom";

export class TypeChatServer {
    Logger: ConsoleLog;
    connectionHandler: ConnectionHandler;
    moduleManager: ModuleManager;
    isInit: boolean = false;
    backend: BackendCom;
    // Length of current authentication queue
    authQueue: number = 0;

    constructor(public ServerConfig) {
        this.Logger = new ConsoleLog("Server");
        this.Logger.startupinfo("Setting up server...");
        this.Logger.info("Loading config...");
        if (ServerConfig != null)
            Config.setConfig(ServerConfig);
        else
            Config.load();
        this.Logger.twotabbedlog("CONFIG OVERVIEW");
        this.Logger.twotabbedlog("SOCKET PORT: " + Config.config.port);
        this.Logger.twotabbedlog("DEBUGGING: " + Config.config.debug);
        this.Logger.startupinfo("Loading modules...");
        this.moduleManager = new ModuleManager();
        this.moduleManager.loadModulesFromModulesPath(this);

        if (!this.moduleManager.checkModules()) {
            let auth = "OK";
            let clientModule = "OK";
            let roomModule = "OK";
            if (this.moduleManager.authModule == null)
                auth = "MISSING OR FAILED!";
            if (this.moduleManager.clientModule == null)
                clientModule = "MISSING OR FAILED!";
            if (this.moduleManager.roomModule == null)
                roomModule = "MISSING OR FAILED!";
            this.Logger.error("One or more required modules have not been loaded: \n    Auth: " + auth + "\n    ClientModule: " + clientModule + "\n    RoomModule: " + roomModule);
            throw new Error('One or more required modules have not been loaded! The server is unable to start.')
        }

        this.Logger.startupinfo("Loading permission rules...");
        // Load permissions
        PermissionManager.loadRules(BasePermissions);

        this.Logger.startupinfo("Loading backend...");
        this.backend = new BackendCom(this);

        // Handle shutdown
        process.on('SIGTERM', () => this.shutdown());
        process.on('SIGINT', () => this.shutdown());

        this.connectionHandler = new ConnectionHandler(this);
    }

    init(readyCallback: any) {
        this.Logger.startupinfo("Initialising Server...");
        this.isInit = true;
        this.moduleManager.roomModule.fetchRooms(readyCallback);
    }

    start() {
        if (!this.isInit) {
            this.Logger.error("Can't start server without initialising it first!");
            return;
        }
        this.Logger.startupinfo("Starting backend...");
        this.backend.start();
        this.Logger.startupinfo("Starting server...");
        this.connectionHandler.start();
        this.Logger.info("The server is now listening for clients!");
    }

    requestAuthSlot(): number {
        let maxQueueSize = Config.get('auth.queueSize', 2);
        if (this.authQueue >= maxQueueSize) {
            let delayIncrease = Math.floor(this.authQueue / maxQueueSize);
            let delay = Config.get('auth.queueDelay', 6) * delayIncrease;
            this.Logger.warning("Auth queue full! Delayed auth request by "+delay+" seconds!");
            this.authQueue++;
            return delay;
        } else {
            this.authQueue++;
            return 0;
        }
    }

    releaseAuthSlot() {
        this.authQueue--;
    }


    broadcast(event, data) {
        this.Logger.debug("Broadcasting a message...");
        for (let client of this.moduleManager.clientModule.clients) {
            client.send(event, data);
        }
    }

    shutdown() {
        this.Logger.warning("Shutting down!");
        this.Logger.info("Stopping socket...");
        this.connectionHandler.socket.close();
        this.Logger.info("Stopping backend...");
        this.backend.stop();
        this.Logger.info("Writing message storage...");
        // TODO: Commit storage
        this.Logger.info("Finishing up...");

        this.Logger.info("🐾 ");
        this.Logger.info("🐾 ");
        this.Logger.info("🐾 ");
        this.Logger.info("🐾 Bye!");
        this.Logger.info("Uptime: " + Formatter.formatSeconds(process.uptime()));
        //process.exit()
    }
}