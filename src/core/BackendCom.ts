/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {TypeChatServer} from "./TypeChatServer";
import {ConsoleLog} from "../helpers/ConsoleLog";
import * as http from "http";
import * as Httpdispatcher from "httpdispatcher";
import {Config} from "./Config";
import {Room} from "../models/base/Room";

export class BackendCom {
    Logger: ConsoleLog;
    dispatcher: any;
    backendSrv: any;

    constructor(public server: TypeChatServer) {
        this.Logger = new ConsoleLog("API");
        this.Logger.info("Preparing backend communication interface on port " + Config.get('backendPort',8888) + "...");
        this.backendSrv = http.createServer((request, response) => this.handleRequest(request, response));

        // @ts-ignore
        this.dispatcher = new Httpdispatcher();

        // register dispatchers
        this.registerDispatcherGET('/status',()=> this.onStatus());
        this.registerDispatcherGET('/',()=> this.onIndex());
        this.registerDispatcherGET('/list/rooms', ()=> this.onRoomList());
        this.registerDispatcherGET('/list/users', ()=> this.onUserList());

        this.registerDispatcherPOST('/updateRoom', (data) => this.onUpdateRoom(data));
        this.registerDispatcherPOST('/leaveRoom', (data) => this.onLeaveRoom(data));
    }

    // noinspection JSMethodCanBeStatic
    onIndex() {
        return {chatServer: Config.version, build: Config.build, message: "Welcome to the backend!"};
    }

    // noinspection JSMethodCanBeStatic
    onStatus() {
        return {online: true, authQueue:this.server.authQueue, clientsConnected:this.server.moduleManager.clientModule.clients.length, usersConnected:this.server.moduleManager.clientModule.getUserList().length};
    }

    onLeaveRoom(data){
        return this.server.moduleManager.roomModule.userLeaveRoom(data.userID,data.roomID);
    }

    onUpdateRoom(data) {
        let room = new Room(data);
        return this.server.moduleManager.roomModule.updateRoom(room);
    }

    onUserList(){
        return this.server.moduleManager.clientModule.getUserList();
    }

    onRoomList(){
        return this.server.moduleManager.roomModule.rooms;
    }

    start() {
        this.backendSrv.listen(Config.get('backendPort',8888), () => {
            this.Logger.info("Backend is now listening to connections!");
        })
    }

    stop() {
        this.backendSrv.close(() => {
            this.Logger.info("Backend stopped!");
        });
    }

    handleRequest(request, response) {
        this.Logger.verbose("REQUESTING " + request.url);
        try {
            this.dispatcher.dispatch(request, response);
        } catch (e) {
            this.Logger.error(e);
            response.end("Error. Check server log.");
        }
    }

    registerDispatcherGET(path: string, reply: any) {
        this.dispatcher.onGet(path, (request, response) => {
            response.writeHead(200, {'Content-Type': 'application/json'});
            if (request.body != null)
                request.body = JSON.parse(request.body);
            response.end(JSON.stringify(reply(request.body)));
        });
    }

    registerDispatcherPOST(path: string, reply: (body) => boolean) {
        this.dispatcher.onPost(path, (request, response) => {
            response.writeHead(200, {'Content-Type': 'application/json'});
            let success = false;
            try {
                this.Logger.debug("Executing with parameters: "+request.body);
                this.Logger.debug("Parsed parameters: "+JSON.stringify(JSON.parse(request.body)));
                success = reply(JSON.parse(request.body));
                this.Logger.debug("Done! Response: "+success);
            } catch (e) {
                this.Logger.debug("Done with errors!");
                this.Logger.error(e);
                this.Logger.error(e.stack);
            }
            response.end(JSON.stringify({"success": success}));
        });
    }

}