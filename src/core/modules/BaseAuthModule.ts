/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import {BaseModuleInterface} from "./BaseModuleInterface";

export interface BaseAuthModule extends BaseModuleInterface {
    /**
     * Function being executed when client sends the 'auth'-command.
     *
     * Should check client authentication and execute onSuccess or onFailure.
     * Should execute onFailure(errorMessage) on error/exception. This message will be transmitted to the client and logged.
     * onSuccess has to include the userModel as JSON
     * @param socketData
     * @param {(userData) => void} onSuccess
     * @param {() => void} onFailure
     */
    authenticate(socketData: any, onSuccess: (userData) => void, onFailure: (reason) => void)
}
