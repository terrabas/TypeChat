/*
 * TypeChat NodeJS Chat Engine
 *
 * @link https://gitlab.com/terrabas/TypeChat
 * @copyright Copyright (c) 2018-2018 Martin Schwarz (TerraBAS) (http://mrtns.at)
 */

import * as jsonminify from 'jsonminify';
import * as fs from "fs";

export class Config {
    static version: string = "TypeChat v1.0.0";
    static build: number = 1;
    static protocolVersion:number = 1;


    static config:any;
    static apiconfig:any;
    static apipath:any;

    static load(){
        let config = null;
        try { config = fs.readFileSync(__dirname+'/config/config.json', 'utf8').toString()} catch (e) {
            throw new Error("Config file read error: "+e);
        }
        Config.config = JSON.parse(jsonminify(config));
        this.updateWildcards();
    }

    static setConfig(config:any){
        Config.config = config;
        this.updateWildcards();
    }

    static get(parm:any, defaultReturn:any){
        let rt = defaultReturn;
        try {
            rt = this.config[parm];
            if(rt == undefined)
                return defaultReturn;
        } catch (e) {
            // ignored
        }
        return rt;
    }

    static updateWildcards(){
        Config.apiconfig = Config.config.sfxapi;
        Config.apipath = Config.apiconfig.protocol + '://' + Config.apiconfig.host;
    }
}